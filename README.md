# My Dotfiles

There's nothing really special about them, and I don't think anyone will be coming here for them except for myself whenever I switch systems or set up a VM. However, they're here if you want 'em.

## Color Schemes

If you're looking for your `newsboat` and `stig` to look like mine, I cannot guarantee that it will work, nor that it will not look hideous in *your* setup unless you're also already using the Dracula theme for `alacritty`. They both required some tweaking using some relative color settings.

## Script Requirements

My goal is to have all my scripts be POSIX-compliant shell scripts without much else, so dependencies outside of what's already installed on just about any normal linux system should be pretty rare.

* [jq](https://github.com/stedolan/jq) - *required to parse JSON for the `sb-weather` script*

The `sb-updates` script is written for arch linux and uses `paru` as the AUR helper. Obviously, if you use `yay` or something else, just change the update command accordingly if required. I may end up writing versions for other distros just to have them available if I get the motivation.

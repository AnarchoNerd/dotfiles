# This is only added to dotfiles because of my vim setup being forced
# into XDG Base Dir compliance (see: https://blog.joren.ga/tools/vim-xdg)
# and just looking at the vimrc could cause confusion without this as
# context

export PATH="$PATH:$HOME/.local/bin:$HOME/scripts"

export BASH_COMPLETION_USER_FILE="$XDG_CONFIG_HOME/bash-completion/bash_completion"
export EDITOR=/usr/bin/nvim
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export HISTFILE="$XDG_DATA_HOME/bash/history"
export LESSHIST=-
export MANPAGER=/usr/bin/most
export MOST_INITFILE="$XDG_CONFIG_HOME/most/.mostrc"
export QT_QPA_PLATFORMTHEME=qt5ct
export TERM=alacritty
export VIMINIT="set nocp | source $XDG_CONFIG_HOME/vim/vimrc"
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
